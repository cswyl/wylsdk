
# WYLSDK iOS使用文档


## 一、概述


### 1.1、说明
   微引力移动应用推广SDK(iOS版)是微引力推出的移动应用推广SDK在iOS平台上的版本(以下简称SDK)。该文档提供了对如何使用SDK的一个详细说明，建议阅读时下载我们的用例工程，代码是最好的使用说明。

如有其他问题可以参考网站的FAQ，或者及时与我们联系。

### 1.2、背景
#### 1.2.1、开发环境
确保您的开发及部署环境符合以下标准

```
* 开发工具：Xcode
* 部署目标：iOS 9.0及以上版本
* SDK版本：官网最新版本
```

#### 1.2.2、注册开户
开发者需在微引力官网上进行注册，在平台审核通过后，开发者就成为了微引力广告联盟的正式会员。

#### 1.2.3、术语介绍
```
* APPID：媒体ID
    您在微引力平台创建媒体(APP)时获得的ID，这个ID是我们在SDK网络中识别您应用的唯一ID
    
* ADID：广告位ID
    您在微引力平台为您的应用创建的某种类型（Banner、开屏、插屏、信息流,全屏视屏，激励视屏，视屏贴片）的广告位ID，每个广告位归属于一个特定APPID，一个APPID可以拥有多个ADID
```

### 1.3、广告位穿建流程
首先，在微引力平台，注册和认证您的应用，在您的应用通过审核后，可以在微引力平台选择“新建广告位”。

步骤说明
```
1. 在创建广告位之前，确保您已经下载最新版SDK并阅读完《说明文档》，了解如何在您的媒体中使用广告位 ID。

2. 选择需要嵌入广告的应用名称。注意此处只可选择已通过审核的应用。

3. 设置广告位的名称及格式。广告位名称用于区分媒体内设置的各个广告位，方便您在后台进行统一管理和查看数据报表；目前提供选择的广告有横幅广告、原生广告、插屏广告、贴片广告，激励视频、内容联盟和开屏广告。

4. 点击完成按钮以后即获得对应该广告位的广告位ID。

5. 成功得到广告位ID以后，您可以在应用代码内使用该ID进行广告联调测试（见“接入代码”部分）。测试时，请替换iOS包名，并替换应用APPID以及对应广告位ID。

6. 通过测试后，与联盟商务确认后，即可发布带有联盟广告位的 iOS 应用。

```
### 1.4、支持的广告位类型
微引力平台支持以下几种广告类型，您可以根据开发需要选择合适的广告：

| 广告类型 | 简介 | 适用场景 |
| --- | --- | --- |
| 开屏广告 | 开屏广告为用户在进入App时展示的全屏广告 | app启动时，使用开屏广告 |
| 插屏广告 | 插屏广告是弹出式广告 | 视频暂停页，打开新页面可弹出插屏广告 |
| 全屏视屏 | 全屏视频广告，该广告的效果播放全屏的视频，视频一定时间后可跳过，无需全程观看完 | 打开新页面或前后台切换可弹出全屏视屏 |
| 横幅(Banner) | 横幅广告(banner 广告)位于一般位于 app 底部或顶部 | 可用于小说底部，详情页面底部，应用界面顶部等 |
| 信息流 | 场景应在应用的内容流中与应用内容穿插展示 | 场景应在应用的内容流中与应用内容穿插展示 |
| 小视频 | 是一种竖版信息流广告，在原生信息流场景中插入竖版样式沉浸式视频；适用于app流量竖版信息流场景 | 适用于app流量竖版信息流场景。短视频 |
| 激励视频 |激励视频广告需要让用户主动选择去观看，广告的效果为观看完毕视频广告，发放奖励给用户  |常出现在游戏单局结束，角色复活，登录奖励领取，服务增值等场景  |
| 视屏贴片 | 贴片广告是常用于视频组件，用于给视频贴片，在视频预加载，暂停或结束时使用。 | 可在视频播缓冲，暂停，结束后，将贴片广告附加到组件上 |
| 悬浮 | 点击浮标，在当前页面以弹窗插屏的形式巧妙的弹出互动活动，在保证用户体验的基础上，将活动前置，提高流量利用率 | 适用于常规首页、二级页面、个人中心等浮标位置 |

## 二、SDK项目部署

**备注**
使用广告SDK，您的iOS应用可以借助微引力广告SDK完成广告创收。本章介绍了在iOS应用中集成广告SDK的方法。

### 2.1、手动部署
**本章会指导您手动将iOS SDK进行集成。如果您没有项目，请先创建一个空白项目。**

#### 2.1.1、导入framework
获取 framework 文件后直接将 {WYLSDK.framework, BUAdSDK.bundle，baidumobadsdk.bundle}文件copy到工程文件夹中，然后在项目中选中项目文件，选择Add Files to “your project name”:

拖入完请确保Copy Bundle Resources中有BUAdSDK.bundle，，baidumobadsdk.bundle否则可能出现icon图片加载不出来的情况。

**添加依赖库**
工程需要在TARGETS -> Build Phases中找到Link Binary With Libraries，点击“+”，依次添加下列依赖库   
* AVFoundation.framework
* AdSupport.framework
* Accelerate.framework
* AudioToolbox.framework
* CoreMedia.framework
* CoreMotion.framework
* CoreGraphics.framework
* CoreImage.framework
* CoreTelephony.framework
* CoreText.framework
* CoreLocation.framework
* CoreTelephony.framework
* ImageIO.framework
* JavaScriptCore.framework
* QuartzCore.framework
* StoreKit.framework
* SafariServices.framework
* SystemConfiguration.framework
* Security.framework
* MapKit.framework
* MediaPlayer.framework
* MobileCoreServices.framework
* MessageUI.framework
* UIKit.framework
* WebKit.framework
* libbz2.tbd
* libc++.tbd
* libiconv.tbd
* libresolv.9.tbd
* libsqlite3.tbd
* libxml2.tbd
* libz.tbd
* libc++abi.tbd



**其它设置**
```
在Target->Build Settings -> Other Linker Flags中添加-ObjC, 字母o和c大写。
Build Settings中Other Linker Flags 增加参数-ObjC、 -l"c++"、 -l"c++abi" 、-l"sqlite3"、-l"z" ，SDK同时支持-all_load
```

### 2.2、CocoaPods集成

SDK从1.0.10版本支持pod方式接入，只需配置pod环境，在podfile文件中加入以下代码即可接入成功。开发者可按照以下方式进行接入

**Podfile**
```
target 'Your APP Xcode Project' do
  pod 'WYLSDK'
end
```

**然后执行**
```
pod install

```

### 2.3、其他设置

#### 2.3.1、添加权限

SDK 不强制获取任何权限和信息，针对单媒体的用户，允许获取idfa和定位权限的，以及在info.plist中配置以下权限，可以有效提升ECPM：

```
// 应用根据实际情况配置
   Privacy - Location When In Use Usage Description
   Privacy - Location Always and When In Use Usage Description
   Privacy - Location Always Usage Description
   Privacy - Location Usage Description
   <key>NSUserTrackingUsageDescription</key>
   <string>This identifier will be used to deliver personalized ads to you.</string>
```
* 工程plist文件设置，点击右边的information Property List后边的 "+" 展开

添加 App Transport Security Settings，先点击左侧展开箭头，再点右侧加号，Allow Arbitrary Loads 选项自动加入，修改值为 YES。 SDK API 已经全部支持HTTPS，但是广告主素材存在非HTTPS情况。
```
<key>NSAppTransportSecurity</key>
    <dict>
         <key>NSAllowsArbitraryLoads</key>
         <true/>
    </dict>
```

#### 2.3.2、注意事项

**请注意SDK中bundle文件的名字请勿修改，每次更新SDK请务必更新bundle文件。**



#### 2.3.3、Swift接入准备
1: 新建桥接头文件（bridge.h，推荐放在工程目录下）。这里我们命名为：WYLSDK-Bridging-Header.h。在这个文件中import我们需要的所有头文件，代码如下：
```
#import <WYLSDK/WYLSDK.h>
```
2: 左侧目录中选中工程名，在 TARGETS->Build Settings-> Swift Compiler - Code Generation -> Objective-C Bridging Header 中输入桥接文件的路径

#### 2.3.4、iOS 14 IDFA适配

**SKAdNetwork（SKAN）**

SKAdNetwork（SKAN） 是 Apple 的归因解决方案，可帮助广告客户在保持用户隐私的同时衡量广告活动。 使用 Apple 的 SKAdNetwork 后，即使 IDFA 不可用，广告网络也可以正确获得应用安装的归因结果。 访问 https://developer.apple.com/documentation/storekit/skadnetwork 了解更多信息。 为了广告转化的归因，所有开发者须设置SKAdNetwork方案的穿山甲SKAdNetwork id。
App Tracking Transparency (ATT) 适用于请求用户授权，访问与应用相关的数据以跟踪用户或设备。 访问 https://developer.apple.com/documentation/apptrackingtransparency了解更多信息。 目前苹果要求在iOS 14.5及以上的版本中必须在弹窗取得用户同意后，才可以追踪用户。对其他版本暂无明确要求，开发者应根据需要配置弹窗 。
将的 SKAdNetwork ID 添加到 info.plist 中，以保证 SKAdNetwork 的正确运行
```
<key>SKAdNetworkItems</key>
  <array>
    <dict>
      <key>SKAdNetworkIdentifier</key>
      <string>238da6jt44.skadnetwork</string>
    </dict>
    <dict>
      <key>SKAdNetworkIdentifier</key>
      <string>x2jnk7ly8j.skadnetwork</string>
    </dict>
    <dict>
      <key>SKAdNetworkIdentifier</key>
      <string>22mmun2rn5.skadnetwork</string>
    </dict>
    <dict>
<key>SKAdNetworkIdentifier</key>
<string>kbd757ywx3.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>mls7yz5dvl.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>4fzdc2evr5.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>4pfyvq9l8r.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>ydx93a7ass.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>cg4yq2srnc.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>p78axxw29g.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>737z793b9f.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>v72qych5uu.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>6xzpu9s2p8.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>ludvb6z3bs.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>mlmmfzh3r3.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>c6k4g5qg8m.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>wg4vff78zm.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>523jb4fst2.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>ggvn48r87g.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>22mmun2rn5.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>3sh42y64q3.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>f38h382jlk.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>24t9a8vw3c.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>hs6bdukanm.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>prcb7njmu6.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>m8dbw4sv7c.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>9nlqeag3gk.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>cj5566h2ga.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>cstr6suwn9.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>w9q455wk68.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>wzmmz9fp6w.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>yclnxrl5pm.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>4468km3ulz.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>t38b2kh725.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>k674qkevps.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>7ug5zh24hu.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>5lm9lj6jb7.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>9rd848q2bz.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>7rz58n8ntl.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>4w7y6s5ca2.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>feyaarzu9v.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>ejvt5qm6ak.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>9t245vhmpl.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>n9x2a789qt.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>44jx6755aq.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>zmvfpc5aq8.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>tl55sbb4fm.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>2u9pt9hc89.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>5a6flpkh64.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>8s468mfl3y.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>glqzh8vgby.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>av6w8kgt66.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>klf5c3l5u5.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>dzg6xy7pwj.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>y45688jllp.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>hdw39hrw9y.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>ppxm28t8ap.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>424m5254lk.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>5l3tpt7t6e.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>uw77j35x4d.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>4dzt52r2t5.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>mtkv5xtk9e.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>gta9lk7p23.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>5tjdwbrq8w.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>3rd42ekr43.skadnetwork</string>
</dict>
<dict>
<key>SKAdNetworkIdentifier</key>
<string>g28c52eehv.skadnetwork</string>
</dict>
  </array>
```

**App Tracking Transparency**

 支持苹果 ATT：从 iOS 14 开始，若开发者设置 App Tracking Transparency 向用户申请跟踪授权，在用户授权之前IDFA 将不可用。 如果用户拒绝此请求，应用获取到的 IDFA 将自动清零，可能会导致您的广告收入的降低

要获取 App Tracking Transparency 权限，请更新您的 Info.plist，添加 NSUserTrackingUsageDescription 字段和自定义文案描述。代码示例：
```
<key>NSUserTrackingUsageDescription</key>
<string>该标识符将用于向您投放个性化广告</string>
```
向用户申请权限时，请调用 requestTrackingAuthorizationWithCompletionHandler:方法。我们建议您申请权限后再请求广告，以便准确的获得用户授权状态。

Objective-C 代码示例

```
#import <AppTrackingTransparency/AppTrackingTransparency.h>
#import <AdSupport/AdSupport.h>
- (void)requestIDFA {
  [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
    // Tracking authorization completed. Start loading ads here.
    // [self loadAd];
  }];
}
```

Swift 代码示例
```
import AppTrackingTransparency
import AdSupport
func requestIDFA() {
  ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
    // Tracking authorization completed. Start loading ads here.
    // loadAd()
  })
}
```

#### 2.3.5、SDK设置

**新的设备标志**

开关新标志能力，该能力默认开启，如果有监管或隐私要求，媒体可根据如下配置，在app内配置是否开启该能力。
```
#import <WYLSDK/WYLSDK.h>
// 开启：
WYLSDKSetting.enableCAIDPermission = YES;
// 关闭：
WYLSDKSetting.enableCAIDPermission = NO;
```


**音频设置**
在播放音频时是否使用SDK内部对AVAudioSession设置的category及options，默认使用，若不使用，SDK内部不做任何处理，由调用方在播放视频时自行设置；

SDK设置的category为AVAudioSessionCategoryAmbient，options为AVAudioSessionCategoryOptionDuckOthers
```
#import <WYLSDK/WYLSDK.h>
WYLSDKSetting.enableDefaultAudioSessionSetting = NO;
```

**GPS信息获取开关**
在已获得GPS权限的前提下，媒体可以选择是否在广告中获取用户的GPS信息，以便获取定向广告。方法如下：
```
#import <WYLSDK/WYLSDK.h>

WYLSDKSetting.enableDefaultAudioSessionSetting = YES; // 获取用户的GPS信息，默认值为NO
```

**SDK 初始化**
SDK 拉取广告之前，必须要进行初始化操作，传入在微引力平台注册得到的 APPID。方法如下：
```
#import <WYLSDK/WYLSDK.h>

[WYLSDKManager registerAppId:@"wyl_1321322"]; 
```

## 三、集成广告


### 3.1、开屏广告

* 基本信息： 开屏广告在 App 启动时展现。用户可以点击广告跳转到广告落地页;或者点击右上角的“跳过”按钮，跳转到 app 内容首页。
* 适用场景： 开屏广告会在您的应用开启时加载，展示完毕后自动关闭并进入您的应用主界面。展示时间和跳过广告样式，可以通过百度联盟(百青藤)后台配置并配合前端自定义修改。
#### 3.1.1、主要API

**WYLSplashAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| splashDidLoad | 广告请求成功 |
| splashSuccessPresentScreen | 开屏广告展示成功 |
| splashDidClicked | 开屏广告点击 |
| splashDidDismissScreen | 开屏广告关闭 |
| splashDidDismissDetailPage | 广告落地页关闭 |
| splashDidFail | 广告发生错误 |


**WYLSplashAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| timeout |NSTimeInterval  | 广告请求超时时间，默认3s，单位s |


| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
| - (void)loadAndshowInWindow:(UIWindow *)window withBottomView:(nullable UIView *)bottomView; | 加载展示广告 |


**加载广告具体示例详见Demo中的Controllers/AdSplashViewController**


### 3.2、插屏广告

* 基本信息： 插屏广告是弹出式广告。
* 适用场景： 视频暂停页，打开新页面可弹出插屏广告。
#### 3.2.1、主要API

**WYLInterstitialAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| interstitialAdDidLoad | 广告请求成功 |
| interstitialAdFailToLoad | 广告请求失败 |
| interstitialAdSuccessPresentScreen | 广告展示成功 |
| interstitialAdDidClicked | 广告点击 |
| interstitialAdDidDismissScreen | 广告关闭 |
| interstitialAdDidDismissDetailPage | 广告落地页关闭 |
| interstitialAdDidFail | 广告发生错误 |


**WYLInterstitialAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |


| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|- (void)loadAd:(NSString *)placeId; | 加载广告 |
|- (void) showAdFromRootViewController:(UIViewController*) rootController;| 展示广告 |


**加载广告具体示例详见Demo中的Controllers/AdInterstitialViewController**

### 3.3、全屏视屏

* 基本信息： 全屏视频广告，该广告的效果播放全屏的视频，视频一定时间后可跳过，无需全程观看完 。
* 适用场景：  打开新页面或前后台切换可弹出全屏视屏。
#### 3.3.1、主要API

**WYLFullVideoAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| fullVideoAdDidLoad | 广告请求成功 |
| fullVideoAdFailToLoad | 广告请求失败 |
| fullVideoAdSuccessPresentScreen | 广告展示成功 |
| fullVideoAdDidClicked | 广告点击 |
| fullVideoAdDidDismissScreen | 广告关闭 |
| fullVideoAdDidDismissDetailPage | 广告落地页关闭 |
| fullVideoAdDidFail | 广告发生错误 |


**WYLInterstitialAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |


| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|- (void)loadAd:(NSString *)placeId; | 加载广告 |
|- (void) showAdFromRootViewController:(UIViewController*) rootController;| 展示广告 |


**加载广告具体示例详见Demo中的Controllers/AdFullVideoViewController**


### 3.4、Banner

* 基本信息：横幅广告(banner 广告)位于一般位于 app 底部或顶部 。
* 适用场景：可用于小说底部，详情页面底部，应用界面顶部等。
#### 3.4.1、主要API

**WYLBannerViewDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| bannerViewAdDidLoad | 广告请求成功 |
| bannerViewAdFailToLoad | 广告请求失败 |
| bannerViewAdSuccessPresentScreen | 广告展示成功 |
| bannerViewAdDidClicked | 广告点击 |
| bannerViewAdDidDismissScreen | 广告关闭 |
| bannerViewAdDidDismissDetailPage | 广告落地页关闭 |
| bannerViewAdDidFail | 广告发生错误 |


**WYLBannerView**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| viewController | UIViewController | 所属Controller |

| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithFrame:(CGRect)frame placeId:(NSString *)placeId; | 初始化广告对象 |
|- (void)loadAdAndShow; | 拉取并展示广告 |


**加载广告具体示例详见Demo中的Controllers/AdBannerViewController**

### 3.5、信息流

* 基本信息：场景应在应用的内容流中与应用内容穿插展示 。
* 适用场景：场景应在应用的内容流中与应用内容穿插展示。
#### 3.5.1、主要API

**WYLFeedAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| - (void)feedAdSuccessToLoad:(WYLFeedAd *)feedAd views:(NSArray<__kindof WYLFeedView *> *)views; | 广告请求成功 |
| - (void)feedAdFailToLoad:(WYLFeedAd *)feedAd error:(NSError *)error; | 广告请求失败 |
| - (void)feedAd:(WYLFeedAd *)feedAd feedAdViewDidShow:(WYLFeedView *)feedView; | 广告展示 |
| - (void)feedAd:(WYLFeedAd *)feedAd feedAdViewDidClick:(WYLFeedView *)feedView; | 广告点击 |
| - (void)feedAd:(WYLFeedAd *)feedAd feedAdViewDislike:(WYLFeedView *)feedView withReason:(NSString*) reason; | 用户不喜欢 |
| - (void)feedAd:(WYLFeedAd *)feedAd feedAdViewDidClosed:(WYLFeedView *)feedView; | 广告关闭 |
| - (void)feedAd:(WYLFeedAd *)feedAd feedAdViewDidCloseDetailController:(WYLFeedView *)feedView; | 广告发生错误 |


**WYLFeedAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| adSize | CGSize | 期望广告尺寸 |

| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|-(void) loadAd:(NSInteger) count; | 拉取广告 |


**加载广告具体示例详见Demo中的Controllers/AdFeedViewController**

### 3.6、竖版视屏信息流(小视频)

* 基本信息：是一种竖版信息流广告，在原生信息流场景中插入竖版样式沉浸式视频 。
* 适用场景：适用于app流量竖版信息流场景。短视频。
#### 3.6.1、主要API

**WYLFeedFullVideoAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| - (void)feedFullVideoAdSuccessToLoad:(WYLFeedFullVideoAd *)fullVideoAd views:(NSArray<__kindof WYLFeedFullVideoView *> *)views | 广告请求成功 |
| - (void)feedFullVideoAdFailToLoad:(WYLFeedAd *)feedAd error:(NSError *)error; | 广告请求失败 |
| - (void)feedFullVideoAd:(WYLFeedFullVideoAd *)fullVideoAd feedAdViewWillShow:(WYLFeedFullVideoView *)videoView; | 广告展示 |
| - (void)feedFullVideoAdPlayerStateDidChanged:(WYLFeedFullVideoAd *)fullVideoAd view:(WYLFeedFullVideoView*) videoView state:(WYLVideoPlayerStatus) playerState; | 广告视频播放状态 |
| - (void)feedFullVideoAdPlayerDidPlayFinish:(WYLFeedFullVideoAd *)fullVideoAd view:(WYLFeedFullVideoView*) videoView; | 广告视频播放完毕 |
| - (void)feedFullVideoAd:(WYLFeedFullVideoAd *)fullVideoAd feedAdViewDidClick:(WYLFeedFullVideoView *)videoView; | 广告点击 |
| - (void)feedFullVideoAd:(WYLFeedFullVideoAd *)fullVideoAd feedAdViewDidClosed:(WYLFeedFullVideoView *)videoView; | 广告关闭 |
| - (void)feedFullVideoAd:(WYLFeedFullVideoAd *)fullVideoAd feedAdViewDidCloseDetailController:(WYLFeedFullVideoView *)videoView; | 广告详情页关闭 |


**WYLFeedFullVideoAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| adSize | CGSize | 期望广告尺寸 |

| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|-(void) loadAd:(NSInteger) count; | 拉取广告 |


**加载广告具体示例详见Demo中的Controllers/AdFeedFullVideoViewController**

### 3.7、激励视屏

* 基本信息：激励视频广告需要让用户主动选择去观看，广告的效果为观看完毕视频广告，发放奖励给用户 。
* 适用场景：常出现在游戏单局结束，角色复活，登录奖励领取，服务增值等场景。
#### 3.7.1、主要API

**WYLRewardVideoAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| - (void)rewardVideoAdDidLoad:(WYLRewardVideoAd *)rewardedVideoAd; | 广告请求成功 |
| - (void)rewardVideoAdVideoDidLoad:(WYLRewardVideoAd *)rewardedVideoAd; | 广告视频数据缓存成功 |
| - (void) rewardVideoAdDidShow:(WYLRewardVideoAd *)rewardedVideoAd; | 广告展示 |
| - (void) rewardVideoAdDidClicked:(WYLRewardVideoAd *)rewardedVideoAd; | 广告点击 |
| - (void) rewardVideoAdDidRewardEffective:(WYLRewardVideoAd *)rewardedVideoAd;| 广告奖励触发 |
| - (void) rewardVideoAdDidPlayFinish:(WYLRewardVideoAd *)rewardedVideoAd; | 广告视屏播放完毕 |
| - (void) rewardVideoAdDidClose:(WYLRewardVideoAd *)rewardedVideoAd; | 广告关闭 |
| - (void) rewardVideoAd:(WYLRewardVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error; | 广告错误 |

**WYLRewardVideoAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| userId | NSString | 可选用户ID |
| rewardName | NSString | 可选激励奖励名称 |
| rewardNum | NSInteger | 可选激励奖励数量 |
| extend | NSDictionary | 可选激励扩展 |
| rewardId | NSString | 只读只读交易ID |

| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|- (void) loadAd; | 拉取广告 |
|- (void) showAdFromViewController:(UIViewController*)rootController; | 展示广告 |

**加载广告具体示例详见Demo中的Controllers/AdRewardVideoViewController**

### 3.8、视屏贴片

* 基本信息：贴片广告是常用于视频组件，用于给视频贴片，在视频预加载，暂停或结束时使用 。
* 适用场景：可在视频播缓冲，暂停，结束后，将贴片广告附加到组件上 。
#### 3.8.1、主要API

**WYLPrerollAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| - (void)prerollAdDidLoad:(WYLPrerollAd *)prerollAd; | 广告请求成功 |
| - (void)prerollAdFailToLoad:(WYLPrerollAd *)prerollAd; | 广告请求失败 |
| - (void) prerollAdDidStart:(WYLPrerollAd *)prerollAd; | 广告展示 |
| - (void) prerollAdDidFinish:(WYLPrerollAd *)prerollAd; | 广告结束 |
| - (void) prerollAdDidClicked:(WYLPrerollAd *)prerollAd; | 广告点击 |
| - (void) prerollAd:(WYLPrerollAd *)prerollAd didFailWithError:(NSError *)error; | 广告错误 |

**WYLPrerollAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| viewController | UIViewController | 所属Controller |
| renderView | UIView | 在那个View 显示 |


| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|- (void) loadAd; | 拉取广告 |
|- (void) closeAd; | 关闭广告 |

**加载广告具体示例详见Demo中的Controllers/AdVideoPrerollViewController**

### 3.9、悬浮

* 基本信息：点击浮标，在当前页面以弹窗插屏的形式巧妙的弹出互动活动，在保证用户体验的基础上，将活动前置，提高流量利用率 。
* 适用场景：适用于常规首页、二级页面、个人中心等浮标位置 。
#### 3.9.1、主要API

**WYLFloatAdDelegate**

| 回调函数 | 回调说明 |
| --- | --- |
| - (void) floatAdDidLoad:(WYLFloatAd *)floatAd; | 广告请求成功 |
| - (void) floatAdSuccessPresentScreen:(WYLFloatAd *)floatAd; | 广告展示 |
| - (void) floatAdDidClicked:(WYLFloatAd *)floatAd; | 广告点击 |
| - (void) floatAdDidClosed:(WYLFloatAd *)floatAd; | 广告关闭 |
| - (void) floatAdDidFail:(WYLFloatAd *)floatAd withError:(NSError *)error; | 广告错误 |

**WYLFloatAd**
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| delegate | id | 委托对象 |
| parentViewController | UIViewController | 所属Controller |
| displayPosition | UIEdgeInsets | 在那个位置 显示 |


| 接口 | 说明 |
| --- | --- |
|- (instancetype)initWithPlaceId:(NSString *)placeId; | 初始化广告对象 |
|- (void) showFloatAd; | 拉取广告 |
|- (void) removeFloatAdView; | 移除广告 |

**加载广告具体示例详见Demo中的Controllers/AdFloatViewController**